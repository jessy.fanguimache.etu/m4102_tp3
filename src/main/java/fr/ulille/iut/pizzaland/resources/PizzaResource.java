package fr.ulille.iut.pizzaland.resources;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;

@Path("/pizzas")
public class PizzaResource {
	
	
	@Context
	public UriInfo uriInfo;
	
	public PizzaResource() {
		
	}
	
	@GET
	public List<PizzaDto> getAll(){
		return new ArrayList<PizzaDto>();
	}
	
	
	 @GET
	 @Path("{id}")
	 public PizzaDto getOnePizza(@PathParam("id") UUID id) {
	      Pizza pizza = new Pizza();
	      pizza.setId(id); // juste pour avoir le même id pour le test
	      pizza.setName("Créole");
	      
	      List<Ingredient> ingredients = new ArrayList<Ingredient>();
	      
	      Ingredient ingredient = new Ingredient();
	      ingredient.setName("saucisse");
	      ingredient.setId(pizza.getId()); // juste pour avoir le même id pour le test
	      ingredients.add(ingredient);
	      
	      
	      pizza.setIngredients(ingredients);
	      
	      System.out.println(pizza);
		  
		  return Pizza.toDto(pizza);
	    }

}
