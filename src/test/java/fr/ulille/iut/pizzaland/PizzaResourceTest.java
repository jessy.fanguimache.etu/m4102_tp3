package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

public class PizzaResourceTest extends JerseyTest {

	private static final Logger LOGGER = Logger.getLogger(IngredientResourceTest.class.getName());
	
	@Override
	protected Application configure() {
		// TODO Auto-generated method stub
		return new ApiV1();

	}


	@Test
	public void testGetEmptyList() {
		Response response = target("/pizzas").request().get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		List<PizzaDto> pizzas;
		pizzas = response.readEntity(new GenericType<List<PizzaDto>>() {});

		assertEquals(0, pizzas.size());
	}
	
	
	@Test
	public void testGetExistingPizza() {
		
		Pizza pizza = new Pizza();
		pizza.setName("Créole");
		
		 List<Ingredient> ingredients = new ArrayList<Ingredient>();
	      
	      Ingredient ingredient = new Ingredient();
	      ingredient.setName("saucisse");
	      ingredient.setId(pizza.getId());
	      ingredients.add(ingredient);
	      
	      pizza.setIngredients(ingredients);
		
		Response res = target("/pizzas").path(pizza.getId().toString()).request(MediaType.APPLICATION_JSON).get();
		assertEquals(Response.Status.OK.getStatusCode(), res.getStatus());
		
		Pizza result = Pizza.fromDto(res.readEntity(PizzaDto.class));
		assertEquals(pizza, result);
		
	}

}
